﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace GraphicPlotter
{
    public partial class Form1 : Form
    {
        private int sizeWidth;
        private PointF[] pointOne;
        private float tempX;


        public Form1()
        {
            InitializeComponent();
            one();
            

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            one();
        }

        void one()
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);
            Pen a = new Pen(Color.Blue, 1);
            Pen b = new Pen(Color.Green, 2);
            Font drawFont = new Font("Arial", 12);
            Font signatureFont = new Font("Arial", 7);
            SolidBrush drawBrush = new SolidBrush(Color.Blue);
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;

            int sizeWidth = this.Width;
            int sizeHeight = this.Height;

            Point center = new Point(((int)(sizeWidth / 2) - 8), (int)((sizeHeight / 2) - 19));

            g.DrawLine(a, 10, center.Y, center.X, center.Y);
            g.DrawLine(a, center.X, center.Y, 2 * center.X - 10, center.Y);
            g.DrawLine(a, center.X, 18, center.X, center.Y);
            g.DrawLine(a, center.X, center.Y, center.X, 2 * center.Y - 18);
            g.DrawString("x", drawFont, drawBrush, new PointF(2 * center.X - 5, center.Y + 1), drawFormat);
            g.DrawString("y", drawFont, drawBrush, new PointF(center.X + 30, 5), drawFormat);

            g.DrawLine(a, center.X, 18, center.X + 5, 20);
            g.DrawLine(a, center.X, 14, center.X - 5, 20);

            int stepForAxes = 25;
            int lengthShtrih = 3;
            int maxValueForAxesX = 8;
            int maxValueForAxesY = 4;
            float oneDelenieX = (float)maxValueForAxesX / ((float)center.X / (float)stepForAxes);

            float oneDelenieY = (float)maxValueForAxesY / ((float)center.Y / (float)stepForAxes);

            for (int i = center.X, j = center.X, k = 1; i < 2 * center.X - 38; j -= stepForAxes, i += stepForAxes, k++)
            {
                g.DrawLine(a, i, center.Y - lengthShtrih, i, center.Y + lengthShtrih);

                g.DrawLine(a, j, center.Y - lengthShtrih, j, center.Y + lengthShtrih);

                if (i < 2 * center.X - 55)
                {
                    g.DrawString((k * oneDelenieX).ToString("0.0"), signatureFont, drawBrush, new PointF(i + stepForAxes + 9, center.Y + 6), drawFormat);
                    g.DrawString(((k * oneDelenieX).ToString("0.0") + " - "), signatureFont, drawBrush, new PointF(j - stepForAxes + 9, center.Y + 6), drawFormat);
                }
            }

            int numOfPoint = 200;

            float[] first = new float[numOfPoint];
            for (int i = 0; i < numOfPoint; i++)
            {
                first[i] = (float)maxValueForAxesX / (float)numOfPoint * (i + 1) - (float)(maxValueForAxesX / 2);
            }

            float[] second = new float[numOfPoint];

            for (int i = 0; i < numOfPoint; i++)
            {
                float x = first[i];
                float y = x / 5 + x * (float)Math.Cos(x) * (float)Math.Sin(x);
                second[i] = y;
            }


            Point[] pointOne = new Point[numOfPoint];
            float tempX = 1 / oneDelenieX * stepForAxes;
            float tempY = 1 / oneDelenieY * stepForAxes;

            chart1.Series.Clear();
            Series series = chart1.Series.Add("My Series");
            series.ChartType = SeriesChartType.Line;
            for (int i = 0; i < numOfPoint; i++)
            {
                series.Points.AddXY(first[i], second[i]);
            }
        }

        private void chart1_Click(object sender, EventArgs e)
        {
           

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            one();
        }
        

    }
}
